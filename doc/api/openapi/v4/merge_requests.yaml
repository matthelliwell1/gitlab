# Markdown documentation: https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/api/merge_requests.md

#/v4/projects/{id}/merge_requests
mergeRequests:
  get:
    description: Lists merge for a project
    summary: List merge for a project
    operationId: mergeRequestsProjects_get
    tags:
      - merge_requests
    parameters:
      - name: id
        in: path
        description: The ID or URL-encoded path of the project owned by the authenticated user.
        required: true
        schema:
          oneOf:
            - type: integer
            - type: string
      - name: state
        in: path
        description: Return all merge requests or just those that are opened, closed, locked, or merged.
        required: false
        schema:
          type: string
          enum: [ opened, closed, locked, merged ]
      - name: order_by
        in: path
        description: Return requests ordered by created_at or updated_at fields. Default is created_at.
        required: false
        schema:
          type: string
          enum: [ created_at, updated_at ]
      - name: sort
        in: path
        description: Return requests sorted in asc or desc order. Default is desc.
        required: false
        schema:
          type: string
          enum: [ asc, desc ]
      - name: milestone
        in: path
        description: Return merge requests for a specific milestone. None returns merge requests with no milestone. Any returns merge requests that have an assigned milestone.
        required: false
        schema:
          type: string
      - name: view
        in: path
        description: If simple, returns the iid, URL, title, description, and basic state of merge request.
        required: false
        schema:
          type: string
      - name: labels
        in: path
        description: Return merge requests matching a comma-separated list of labels. None lists all merge requests with no labels. Any lists all merge requests with at least one label. Predefined names are case-insensitive.
        required: false
        schema:
          type: string
      - name: with_labels_details
        in: path
        description: |
          If true, response returns more details for each label in labels field: :name, :color, :description, :description_html, :text_color. Default is false.
          Introduced in GitLab 12.7.
        required: false
        schema:
          type: boolean
      - name: with_merge_status_recheck
        in: path
        description: |
          If true, this projection requests (but does not guarantee) that the merge_status field be recalculated asynchronously. Default is false. Introduced in
          GitLab 13.0.
        required: false
        schema:
          type: boolean
      - name: created_after
        in: path
        description: Return merge requests created on or after the given time. Expected in ISO 8601 format (2019-03-15T08:00:00Z)
        required: false
        schema:
          type: string
      - name: created_before
        in: path
        description: Return merge requests created on or before the given time. Expected in ISO 8601 format (2019-03-15T08:00:00Z)
        required: false
        schema:
          type: string
      - name: updated_after
        in: path
        description: Return merge requests updated on or after the given time. Expected in ISO 8601 format (2019-03-15T08:00:00Z)
        required: false
        schema:
          type: string
      - name: updated_before
        in: path
        description: Return merge requests updated on or before the given time. Expected in ISO 8601 format (2019-03-15T08:00:00Z)
        required: false
        schema:
          type: string
      - name: scope
        in: path
        description: |
          Return merge requests for the given scope: created_by_me, assigned_to_me or all. Defaults to created_by_me.
        required: false
        schema:
          type: string
      - name: author_id
        in: path
        description: Returns merge requests created by the given user id. Mutually exclusive with author_username. Combine with scope=all or scope=assigned_to_me.
        required: false
        schema:
          type: integer
      - name: author_username
        in: path
        description: Returns merge requests created by the given username. Mutually exclusive with author_id. Introduced in GitLab 12.10.
        required: false
        schema:
          type: string
      - name: assignee_id
        in: path
        description: Returns merge requests assigned to the given user id. None returns unassigned merge requests. Any returns merge requests with an assignee.
        required: false
        schema:
          type: integer
      - name: approver_ids
        in: path
        description: |
          Returns merge requests which have specified all the users with the given ids as individual approvers. None returns merge requests without approvers. Any returns merge
          requests with an approver.
        required: false
        schema:
          type: array
          items:
            type: integer
      - name: approved_by_ids
        in: path
        description: |
          Returns merge requests which have been approved by all the users with the given ids (Max: 5). None returns merge requests with no approvals. Any returns merge requests
          with an approval.
        required: false
        schema:
          type: array
          items:
            type: integer
      - name: reviewer_id
        in: path
        description: |
          Returns merge requests which have the user as a reviewer with the given user id. None returns merge requests with no reviewers. Any returns merge requests with any
          reviewer. Mutually exclusive with reviewer_username.
        required: false
        schema:
          type: integer
      - name: reviewer_username
        in: path
        description: |
          Returns merge requests which have the user as a reviewer with the given username. None returns merge requests with no reviewers. Any returns merge requests with any
          reviewer. Mutually exclusive with reviewer_id. Introduced in GitLab 13.8.
        required: false
        schema:
          type: string
      - name: my_reaction_emoji
        in: path
        description: |
          Return merge requests reacted by the authenticated user by the given emoji. None returns issues not given a reaction. Any returns issues given at least one reaction.
        required: false
        schema:
          type: string
      - name: source_branch
        in: path
        description: Return merge requests with the given source branch.
        required: false
        schema:
          type: string
      - name: target_branch
        in: path
        description: Return merge requests with the given target branch.
        required: false
        schema:
          type: string
      - name: search
        in: path
        description: Search merge requests against their title and description.
        required: false
        schema:
          type: string
      - name: in
        in: path
        description: Modify the scope of the search attribute. title, description, or a string joining them with comma. Default is title,description.
        required: false
        schema:
          type: string
      - name: wip
        in: path
        description: Filter merge requests against their wip status. yes to return only draft merge requests, no to return non-draft merge requests.
        required: false
        schema:
          type: string
      - name: not
        in: path
        description: |
          Return merge requests that do not match the parameters supplied. Accepts: labels, milestone, author_id, author_username, assignee_id, assignee_username, reviewer_id,
          reviewer_username, my_reaction_emoji.
        required: false
        schema:
          type: Hash
      - name: environment
        in: path
        description: Returns merge requests deployed to the given environment. Expected in ISO 8601 format (2019-03-15T08:00:00Z)
        required: false
        schema:
          type: string
      - name: deployed_before
        in: path
        description: Return merge requests deployed before the given date/time. Expected in ISO 8601 format (2019-03-15T08:00:00Z)
        required: false
        schema:
          type: string
      - name: deployed_after
        in: path
        description: Return merge requests deployed after the given date/time. Expected in ISO 8601 format (2019-03-15T08:00:00Z
        required: false
        schema:
          type: string

    responses:
      '401':
        description: Unauthorized operation
      '200':
        description: Successful operation
        content:
          application/json:
            schema:
              title: MergeRequestsResponse
              type: array
              items:
                type: object
                properties:
                  id:
                    type: integer
                  iid:
                    type: integer
                  project_id:
                    type: integer
                  title:
                    type: string
                  description:
                    type: string
                  state:
                    type: string
                  merged_by:
                    type: object
                    properties:
                      id:
                        type: integer
                      name:
                        type: string
                      username:
                        type: string
                      state:
                        type: string
                      avatar_url:
                        type: string
                      web_url:
                        type: string
                  merged_at:
                    type: string
                  closed_by:
                    type: string
                    nullable: true
                  closed_at:
                    type: string
                    nullable: true
                  created_at:
                    type: string
                    format: date-time
                  updated_at:
                    type: string
                    format: date-time
                  target_branch:
                    type: string
                  source_branch:
                    type: string
                  upvotes:
                    type: integer
                  downvotes:
                    type: integer
                  author:
                    type: object
                    properties:
                      id:
                        type: integer
                      name:
                        type: string
                      username:
                        type: string
                      state:
                        type: string
                      avatar_url:
                        type: string
                        nullable: true
                      web_url:
                        type: string
                  assignee:
                    type: object
                    properties:
                      id:
                        type: integer
                      name:
                        type: string
                      username:
                        type: string
                      state:
                        type: string
                      avatar_url:
                        type: string
                        nullable: true
                      web_url:
                        type: string
                  assignees:
                    type: array
                    items:
                      type: object
                      properties:
                        name:
                          type: string
                        username:
                          type: string
                        id:
                          type: integer
                        state:
                          type: string
                        avatar_url:
                          type: string
                        web_url:
                          type: string
                  reviewers:
                    type: array
                    items:
                      type: object
                      properties:
                        id:
                          type: integer
                        name:
                          type: string
                        username:
                          type: string
                        state:
                          type: string
                        avatar_url:
                          type: string
                        web_url:
                          type: string
                  source_project_id:
                    type: integer
                  target_project_id:
                    type: integer
                  labels:
                    type: array
                    items:
                      type: string
                  draft:
                    type: boolean
                  work_in_progress:
                    type: boolean
                  milestone:
                    type: object
                    properties:
                      id:
                        type: integer
                      iid:
                        type: integer
                      project_id:
                        type: integer
                      title:
                        type: string
                      description:
                        type: string
                      state:
                        type: string
                      created_at:
                        type: string
                      updated_at:
                        type: string
                      due_date:
                        type: string
                        format: date
                      start_date:
                        type: string
                        format: date
                      web_url:
                        type: string
                  merge_when_pipeline_succeeds:
                    type: boolean
                  merge_status:
                    type: string
                    enum: [ unchecked, checking, can_be_merged, cannot_be_merged, cannot_be_merged_recheck ]
                  sha:
                    type: string
                  merge_commit_sha:
                    type: string
                    nullable: true
                  squash_commit_sha:
                    type: string
                    nullable: true
                  user_notes_count:
                    type: integer
                  discussion_locked:
                    type: string
                    nullable: true
                  should_remove_source_branch:
                    type: boolean
                  force_remove_source_branch:
                    type: boolean
                  allow_collaboration:
                    type: boolean
                  allow_maintainer_to_push:
                    type: boolean
                  web_url:
                    type: string
                  references:
                    type: object
                    properties:
                      short:
                        type: string
                      relative:
                        type: string
                      full:
                        type: string
                  time_stats:
                    type: object
                    properties:
                      time_estimate:
                        type: integer
                      total_time_spent:
                        type: integer
                      human_time_estimate:
                        type: string
                        nullable: true
                      human_total_time_spent:
                        type: string
                        nullable: true
                  squash:
                    type: boolean
                  task_completion_status:
                    type: object
                    properties:
                      count:
                        type: integer
                      completed_count:
                        type: integer
            example:
              - id: 1
                iid: 1
                project_id: 3
                title: test1
                description: fixed login page css paddings
                state: merged
                merged_by:
                  id: 87854
                  name: Douwe Maan
                  username: DouweM
                  state: active
                  avatar_url: https://gitlab.example.com/uploads/-/system/user/avatar/87854/avatar.png
                  web_url: https://gitlab.com/DouweM
                merged_at: '2018-09-07T11:16:17.520Z'
                closed_by:
                closed_at:
                created_at: '2017-04-29T08:46:00Z'
                updated_at: '2017-04-29T08:46:00Z'
                target_branch: master
                source_branch: test1
                upvotes: 0
                downvotes: 0
                author:
                  id: 1
                  name: Administrator
                  username: admin
                  state: active
                  avatar_url:
                  web_url: https://gitlab.example.com/admin
                assignee:
                  id: 1
                  name: Administrator
                  username: admin
                  state: active
                  avatar_url:
                  web_url: https://gitlab.example.com/admin
                assignees:
                  - name: Miss Monserrate Beier
                    username: axel.block
                    id: 12
                    state: active
                    avatar_url: http://www.gravatar.com/avatar/46f6f7dc858ada7be1853f7fb96e81da?s=80&d=identicon
                    web_url: https://gitlab.example.com/axel.block
                reviewers:
                  - id: 2
                    name: Sam Bauch
                    username: kenyatta_oconnell
                    state: active
                    avatar_url: https://www.gravatar.com/avatar/956c92487c6f6f7616b536927e22c9a0?s=80&d=identicon
                    web_url: http://gitlab.example.com//kenyatta_oconnell
                source_project_id: 2
                target_project_id: 3
                labels:
                  - Community contribution
                  - Manage
                draft: false
                work_in_progress: false
                milestone:
                  id: 5
                  iid: 1
                  project_id: 3
                  title: v2.0
                  description: Assumenda aut placeat expedita exercitationem labore sunt enim earum.
                  state: closed
                  created_at: '2015-02-02T19:49:26.013Z'
                  updated_at: '2015-02-02T19:49:26.013Z'
                  due_date: '2018-09-22'
                  start_date: '2018-08-08'
                  web_url: https://gitlab.example.com/my-group/my-project/milestones/1
                merge_when_pipeline_succeeds: true
                merge_status: can_be_merged
                sha: '8888888888888888888888888888888888888888'
                merge_commit_sha:
                squash_commit_sha:
                user_notes_count: 1
                discussion_locked:
                should_remove_source_branch: true
                force_remove_source_branch: false
                allow_collaboration: false
                allow_maintainer_to_push: false
                web_url: http://gitlab.example.com/my-group/my-project/merge_requests/1
                references:
                  short: "!1"
                  relative: "!1"
                  full: my-group/my-project!1
                time_stats:
                  time_estimate: 0
                  total_time_spent: 0
                  human_time_estimate:
                  human_total_time_spent:
                squash: false
                task_completion_status:
                  count: 0
                  completed_count: 0
                has_conflicts: false
                blocking_discussions_resolved: true
